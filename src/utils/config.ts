import * as dotenv from 'dotenv'

let path: string
console.log(process.env.NODE_ENV)
switch (process.env.NODE_ENV) {
  case 'test':
    path = `${process.env.PWD}/.env.test`
    break
  case 'production':
    path = `${process.env.PWD}/.env.dev`
    break
  default:
    path = `${process.env.PWD}/.env`
}

dotenv.config({ path: path })

export const DISCORD_TOKEN = process.env.DISCORD_TOKEN
export const PROJECT_ID = process.env.PROJECT_ID
export const AUTH_TOKEN = process.env.AUTH_TOKEN