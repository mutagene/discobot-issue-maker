import * as env from './utils/config'
import fetch from 'node-fetch'
import * as D from 'discord.js'
const bot = new D.Client()

// https://discord.js.org/#/
// https://docs.gitlab.com/ee/api/issues.html

export const issue = (msg: D.Message, title: string): void => {

  if (!title.length) {
    return
  }

  fetch(`https://gitlab.com/api/v4/projects/${env.PROJECT_ID}/issues/`, {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + env.AUTH_TOKEN,
    },
    body: JSON.stringify({
      title: title,
      labels: 'toRedact', // TODO: pamamétrer ça
    })
  })
    .then(res => {
      if (res.ok && res.status === 201) {
        return res.json()
      } else {
        console.log(res)
      }
    })
    .then(json => {
      console.log(`Issue created : ${json.web_url}`)
      msg.reply(`Issue created : <${json.web_url}> ${title}`) // <link> avoid link preview
    })
    .then(
      () => msg.delete()
    )
    .catch( err => {
      console.log('Issue failed')
      console.log(err)
    })
}

const answer = (msg: D.Message) => {
  if (msg.content.substring(0, 1) !== '!') {
    return
  }

  const argAndCmd = msg.content.substring(1).split(' ')
  const cmd = argAndCmd[0]
  const args = argAndCmd.splice(1)

  switch(cmd) {
  // !ping
    case 'ping':
      msg.reply('Pong!')
      break
    case 'issue':
      issue(msg, args.join(' '))
  }
}

bot.on('ready', () => {
  console.log(`Logged in as ${bot.user?.tag}!`)
})
bot.on('message', answer)

bot.login(process.env.DISCORD_TOKEN)