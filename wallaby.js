process.env.NODE_ENV = 'test';

module.exports = function (wallaby) {

  return {
    files: [
      'src/*.ts',
      'src/**/*.ts',
      'tsconfig.json',
      '!src/**/*.spec.ts',
    ],

    filesWithNoCoverageCalculated: [
      'src/*/*.interfaces.ts',
    ],

    tests: [
      'tests/**/*.spec.ts'
    ],

    env: {
      type: 'node',
      runner: 'node',
      // params: {
      //   env: 'NODE_ENV=test;'
      // }
    },

    testFramework: 'mocha',


    // workers: {  // à voir si nécessaire
    //   initial: 6,
    //   regular: 4,
    //   restart: false,
    // }
    // TODO: ok pour wallaby, mais mocha lancé via npm ET ide : nok
    // setup: wallaby => {
    //   global.expect = require('chai').expect;
    // },
  };


}