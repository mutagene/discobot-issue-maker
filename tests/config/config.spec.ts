import { expect } from 'chai'
import * as env from '../../src/utils/config'

describe('test a test', () => {

  it('should have a test env', () => {
    expect(parseInt(env.PROJECT_ID as string)).to.be.eq(666)
  })

})
